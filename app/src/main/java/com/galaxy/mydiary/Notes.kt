package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_notes.*
import java.lang.Exception

class Notes : AppCompatActivity() {
var list : ArrayList<DiaryItem> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_notes)

        readNotes()

        addNote.setOnClickListener { startActivity(Intent(this, NewNote::class.java)) }
        profile.setOnClickListener { startActivity(Intent(this, ActivityProfile::class.java)) }
    }

    private fun readNotes()
    {
        try{
        FirebaseDatabase.getInstance().reference.child("Users").child(FirebaseAuth.getInstance().currentUser!!.uid).child("Notes")
            .addValueEventListener(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                    Toast.makeText(applicationContext, p0.message, Toast.LENGTH_LONG).show()
                }

                override fun onDataChange(p0: DataSnapshot) {
                    list.clear()
                    for(i : DataSnapshot in p0.children)
                    {
                        val title = i.child("Title").value!!.toString()
                        val body = i.child("Body").value!!.toString()
                        val date = i.child("Date").value!!.toString()

                        list.add(DiaryItem(i.key.toString(), title, date, body))
                    }

                    arlist.adapter = DiaryAdapter(applicationContext, sortedList())
                }

            })
    } catch (e : Exception)
        {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun sortedList() : ArrayList<DiaryItem>
    {
        var i = ArrayList<DiaryItem>()
        var index = 0

        if(list.size >= 1) {
            for (item: DiaryItem in list) {
                if(index < list.size - 1) {
                    if (list[index].date < list[index + 1].date) {
                        val temp = list[index]
                        list[index] = list[index + 1]
                        list[index + 1] = temp
                        index++
                    }
                }
            }
        }

        return list
    }
}
