package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_view_diary.*

class ViewDiary : AppCompatActivity() {
    var id : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_view_diary)

        id = intent.getStringExtra("id")!!

        readCurrent()

        deleteItem.setOnClickListener { delete() }
        editItem.setOnClickListener { edit() }
    }

    private fun readCurrent()
    {
        if(id.isNotEmpty())
        {
            FirebaseDatabase.getInstance().reference.child("Users")
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .child("Notes")
                .child(id)
                .addValueEventListener(object : ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError) {
                     //   TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

                        if(p0.hasChild("Title"))
                            viewTitle.text = p0.child("Title").value!!.toString()
                        if(p0.hasChild("Date"))
                            viewDate.text = p0.child("Date").value!!.toString()
                        if(p0.hasChild("Body"))
                            viewBody.text = p0.child("Body").value!!.toString()
                    }

                })
        }
    }

    private fun delete()
    {
        AlertDialog.Builder(this)
            .setTitle("Delete item")
            .setMessage("You are about to delete this item, Are you sure?")
            .setPositiveButton("Yes"
            ) { _, _ ->
                Toast.makeText(applicationContext, "Your note has been deleted!", Toast.LENGTH_LONG).show()

                FirebaseDatabase.getInstance().reference.child("Users").child(FirebaseAuth.getInstance().currentUser!!.uid)
                    .child("Notes").child(id).removeValue()
                    .addOnCompleteListener {
                        if(it.isSuccessful)
                            Toast.makeText(this, "Your Diary item has been deleted!", Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, "Error: " + it.result, Toast.LENGTH_LONG).show()
                    }
                finish()
            }
            .setNegativeButton("No")
            {_, _ ->

            }
            .show()
    }

    private fun edit()
    {
        startActivity(Intent(this, NewNote::class.java)
            .putExtra("edit", true)
            .putExtra("Title", viewTitle.text.toString())
            .putExtra("Body", viewBody.text.toString()))
    }
}
