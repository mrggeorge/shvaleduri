package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_select_lang.*

class SelectLang : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_select_lang)

        GeorgianLang.startAnimation(AnimationUtils.loadAnimation(this, R.anim.first_appear))

        EnglishLang.startAnimation(AnimationUtils.loadAnimation(this, R.anim.first_appear))


        EnglishLang.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))}
    }
}
