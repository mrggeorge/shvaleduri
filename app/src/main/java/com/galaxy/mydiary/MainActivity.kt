package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var auth : FirebaseAuth = FirebaseAuth.getInstance()
//    var user : FirebaseUser = auth.currentUser!!
    // OnCreate ფუნქცია
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_main)

    if(auth.currentUser != null){
        startActivity(Intent(this, Notes::class.java))
        Toast.makeText(this, "Login Success!", Toast.LENGTH_LONG).show()
        finish()
    }

    login_btn.setOnClickListener { check() }
        register.setOnClickListener {
            startActivity(Intent(this, Register::class.java))
        }
    }


    // ამოწმებს მოცემული ველები შევსებულია თუ არა
    private fun check()
    {
        if (login_email.text.toString().isEmpty()) {
                login_email.setError("This field is required!")
            }

        if (login_password.text.toString().isEmpty()) {
                login_password.setError("This field is required!")
            }

        if(login_email.text.toString().isNotEmpty() && login_password.text.toString().isNotEmpty())
        {
            signIn()
        }
    }



    // შეჰყავს მომხმარებელი სისტემაში მეილითა და პაროლით, თუ იგი არსებობს,
    // არარსებობის შემთხვევაში ბეჭდავს ერორს.
    private fun signIn()
    {
        auth.signInWithEmailAndPassword(login_email.text.toString(), login_password.text.toString())
            .addOnCompleteListener {
                if(it.isSuccessful)
                {
                    Toast.makeText(this, "Login Success!", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, Notes::class.java))
                    finish()
                } else {
                    Toast.makeText(this, "Something went wrong!", Toast.LENGTH_LONG).show()
                }
            }
    }
}
