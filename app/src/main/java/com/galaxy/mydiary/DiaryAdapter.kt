package com.galaxy.mydiary

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DiaryAdapter(c : Context, list : ArrayList<DiaryItem>) : RecyclerView.Adapter<DiaryAdapter.ViewHolder>() {
    val c : Context = c
    val list : ArrayList<DiaryItem> = list

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        var i : DiaryItem = list.get(position)

        holder.title.text = i.title
        holder.date.text = i.date
        holder.body.text = i.body

        holder.itemView.setOnClickListener { c.startActivity(Intent(c, ViewDiary::class.java).putExtra("id", i.id).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.diary_item, parent, false))
    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return list.size
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val title = itemView.findViewById<TextView>(R.id.note_title)
        val date = itemView.findViewById<TextView>(R.id.note_date)
        val body = itemView.findViewById<TextView>(R.id.note_body)
    }


    // ერთეულის დაჭერისას ამოშლილი მენიუ
    private fun itemMenu()
    {

    }

    private fun edit()
    {

    }
}