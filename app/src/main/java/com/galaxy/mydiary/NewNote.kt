package com.galaxy.mydiary

import android.app.Notification
import android.content.DialogInterface
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.Typeface.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.text.style.TextAppearanceSpan
import android.text.style.TypefaceSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.resources.TextAppearance
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_new_note.*
import kotlinx.android.synthetic.main.diary_item.*
import java.lang.Exception
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.time.format.TextStyle
import java.util.*
import kotlin.collections.HashMap

class NewNote : AppCompatActivity() {

    lateinit var note : String;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_new_note)

        var check = intent.getBooleanExtra("edit", false)
        if(check)
            parseEdit()
        // DONE ღილაკზე დაჭერისას დღიურს ატვირთავს ბაზაში
        finishNote.setOnClickListener {
            upload()
        }
    }

    // ტვირთავს ჩაწერილ დღიურს Firebase მონაცემთა ბაზაში
    private fun upload()
    {
        try {
            val date = SimpleDateFormat("dd.MM.yyyy    hh:mm:ss a", Locale.getDefault()).format(Date()).toString()
            if (newNote_title.text.toString().isNotEmpty() && newNote_body.text.toString().isNotEmpty()) {
                val a = HashMap<String, String>()
                a["Title"] = newNote_title.text.toString()
                a["Date"] = date
                a["Body"] = newNote_body.text.toString()

                FirebaseDatabase.getInstance().reference.child("Users")
                    .child(FirebaseAuth.getInstance().currentUser!!.uid)
                    .child("Notes")
                    .push()
                    .setValue(a)
            }
            Toast.makeText(this, "Your diary has been added!", Toast.LENGTH_LONG).show()
            finish()
        } catch (e : Exception)
        {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    // "უკან" ღილაკზე დაჭერისას ამოაგდებს დიალოგს სადაც მომხმარებელს ეკითხება
    // სურს თუ არა ყველა ცვლილების წაშლა
    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Where are you going?")
            .setMessage("You have unsaved changes, if you go back, your note will be discarded. Are you sure you want to exit?")
            .setPositiveButton("Discard"
            ) { _, _ ->
                Toast.makeText(applicationContext, "All the changes discarded!", Toast.LENGTH_LONG).show()
                finish()
            }
            .setNegativeButton("Cancel")
            {_, _ ->

            }
            .show()
    }

    private fun parseEdit()
    {
        var title = intent.getStringExtra("Title")
        var body = intent.getStringExtra("Body")

        newNote_title.setText(title)
        newNote_body.setText(body)
    }
    // ტექსტის სტილის ღილაკებზე დაკლიკების მსმენელი
    fun changeTextStyle(v : View)
    {
        when(v.id){
            R.id.makeBold -> makeBold()
            R.id.makeItalic -> makeItalic()
            R.id.makeNormal -> makeNormal()
        }
    }

    // მონიშნულ ტექსტს ამუქებს
    private fun makeBold()
    {
        var ssb : SpannableStringBuilder = SpannableStringBuilder(newNote_body.text.toString())
        ssb.setSpan(StyleSpan(Typeface.BOLD), newNote_body.selectionStart, newNote_body.selectionEnd, 1)
        newNote_body.text = ssb
    }

    // მონიშნულ ტექსტს ხრის პიზას კოშკივით
    private fun makeItalic()
    {

    }

    // ტექსტს აბრუნებს საწყის სტილზე
    private fun makeNormal()
    {

    }
}
