package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_splash)

        splash_text.startAnimation(AnimationUtils.loadAnimation(this, R.anim.first_appear))

        Handler().postDelayed(Runnable {
            startActivity(Intent(this, SelectLang::class.java))
            finish()
        }, 3000)
    }
}
