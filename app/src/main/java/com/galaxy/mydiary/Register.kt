package com.galaxy.mydiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {
    var auth : FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_register)

        back_btn.setOnClickListener { finish() }
        signin.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)); finish() }
        startRegister.setOnClickListener { Register() }
    }

    // Firebase რეგისტრაცია
    private fun Register()
    {
        auth.createUserWithEmailAndPassword(enter_email.text.toString(), repeat_pass.text.toString())
            .addOnCompleteListener {
                if(it.isSuccessful)
                {
                    uploadInfo()
                } else {
                    Toast.makeText(this, it.exception.toString(), Toast.LENGTH_LONG).show()
                }
            }
    }


    // მონაცემთა ბაზაში ინფორმაციის ატვირთვა
    private fun uploadInfo()
    {
        val hm = HashMap<String, String>()
        hm.put("E-Mail", auth.currentUser!!.email.toString())
        hm.put("Name", enter_name.text.toString())
        FirebaseDatabase.getInstance().getReference().child("Users").child(auth.currentUser!!.uid)
            .setValue(hm)
            .addOnCompleteListener {
                if (it.isSuccessful)
                {
                    Toast.makeText(this, "Register Success!", Toast.LENGTH_LONG).show()
                } else {

                    Toast.makeText(this, it.exception.toString(), Toast.LENGTH_LONG).show()
                }
            }
    }
}
