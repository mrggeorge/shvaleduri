package com.galaxy.mydiary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_profile.*

class ActivityProfile : AppCompatActivity() {
    var database : DatabaseReference = FirebaseDatabase.getInstance().reference.child("Users")
        .child(FirebaseAuth.getInstance().currentUser!!.uid)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_profile)

        read()
        saveChanges.setOnClickListener { checkEmpty() }
    }

    private fun checkEmpty()
    {
        if(edit_name.text.toString().isNotEmpty())
        {
            database.child("Name").setValue(edit_name.text.toString())
        }

        if(edit_password.text.toString().isNotEmpty() && repeat_edit_password.text.toString().isNotEmpty())
        {
            if(edit_password.text.toString() == repeat_edit_password.text.toString())
            {
                FirebaseAuth.getInstance().currentUser!!.updatePassword(repeat_edit_password.text.toString())
            }
        }

        if(profimig_url.text.toString().isNotEmpty())
        {
            database.child("Image").setValue(profimig_url.text.toString())
            Glide.with(this).load(profimig_url.text.toString()).into(profimig)
        }

        if(abtMe.text.toString().isNotEmpty())
        {
            database.child("About").setValue(abtMe.text.toString())
        }
    }

    private fun read()
    {
        database.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

                if(p0.child("Image").exists())
                    Glide.with(applicationContext).load(p0.child("Image").value.toString()).into(profimig)
                if(p0.child("Name").exists())
                    edit_name.setText(p0.child("Name").value.toString())
                if(p0.child("About").exists())
                    abtMe.setText(p0.child("About").value.toString())

            }

        })
    }
}

