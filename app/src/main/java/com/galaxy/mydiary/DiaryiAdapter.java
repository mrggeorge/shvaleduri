package com.galaxy.mydiary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DiaryiAdapter extends RecyclerView.Adapter<DiaryiAdapter.ViewHolder> {
    Context c;
    ArrayList<DiaryItem> list;

    public DiaryiAdapter(Context c, ArrayList<DiaryItem> i)
    {
        this.c = c;
        this.list = i;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.diary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DiaryItem i = list.get(position);

        holder.title.setText(i.getTitle());
        holder.date.setText(i.getDate());
        holder.body.setText(i.getBody());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title, date, body;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.note_title);
            date = itemView.findViewById(R.id.note_date);
            body = itemView.findViewById(R.id.note_body);
        }
    }
}
